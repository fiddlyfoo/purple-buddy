/**
 * LEDs
 *
 * @since 0.0.2
 */

/** @file */


#ifndef PURPLEBUDDY_LEDS_H
#define PURPLEBUDDY_LEDS_H

/**
 * LED pins
 */
//extern const uint8_t PIN_RGB;   // 7

/**
 * RGB Constants
 *
 * Given 3 RGB LEDs, assign each LED to some status
 */
//extern const uint8_t RGB_PWR;    // 0 -- Power Status
//extern const uint8_t RGB_TMP;    // 1 -- Temperature State
//extern const uint8_t RGB_NET;    // 2 -- Network Status (Conn/Disconn)
//extern const uint8_t BRIGHTNESS; // 0 - 255

/**
 * Colors
 *
 * 0xRRGGBB
 */
extern const uint8_t PURPLE; // 0xff00ff
extern const uint8_t BLUE;   // 0x0000ff
extern const uint8_t GREEN;  // 0x00ff00
extern const uint8_t ORANGE; // 0xff2800
extern const uint8_t RED;    // 0xff0000
extern const uint32_t CYAN;   // 0x00ffff

extern const uint8_t MIST; // 17
extern const uint8_t DEW; // 18
extern const uint8_t AIR; // 0
extern const uint8_t PETRICHOR; // 19
extern const uint8_t OCEAN; // 20
extern const uint8_t H_NOTDET; // 21

#endif //PURPLEBUDDY_LEDS_H
