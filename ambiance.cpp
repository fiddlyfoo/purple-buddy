/**
 * Ambiance
 *
 * @since 0.0.2
 */
/** @file */

#include <SimpleDHT.h>
#include "ambiance.h"

#define PIN_AMBIANCE 8

unsigned short READ_AMBIANCE_TIMEOUT  = 5000;
unsigned long READ_AMBIANCE_DEBOUNCE = 0;
unsigned short WRITE_AMBIANCE_TIMEOUT = 60000;
unsigned long WRITE_AMBIANCE_DEBOUNCE = 0;
const uint8_t AMBIANCE_EEPROM_INDEX = 25;
uint8_t NUM_ENTRIES = 0;

SimpleDHT22 dht22(PIN_AMBIANCE);

// temp threshold values -- read on boot. Do not set manually.
byte MJR_U = 0;
byte MNR_U = 0;
byte COMFY = 0; // do not use anymore.
byte MNR_O = 0;
byte MJR_O = 0;

// humidity threshold values
byte h_MJR_U = 40;
byte h_MNR_U = 55;
byte h_MNR_O = 70;
byte h_MJR_O = 85;

int8_t temperature_c = 0;
int8_t temperature_f = 0;
byte humidity      = 0;
uint8_t threshold  = 0x00;
uint8_t h_threshold = 0x00;

void update_dht22() {
  unsigned short err = SimpleDHTErrSuccess;

  if ((err = dht22.read(&temperature_c, &humidity, NULL)) != SimpleDHTErrSuccess) {
    Serial.print("DHT22Err");
    temperature_f = 127;
    humidity = 127;
    threshold = NOTDET;
    h_threshold = NOTDET;
  } else {
    temperature_f = fahrenheit(temperature_c);
    set_h_threshold();
  }
}

void set_h_threshold() {
  if (humidity <= h_MJR_U) {
    // Alm:MjrU
    threshold = MIST;
  } else if (humidity > MJR_U && humidity <= MNR_U) {
    // Alm:MnrU
    threshold = DEW;
  } else if (humidity > MNR_U && humidity <= MNR_O) {
    // Clr:Humidity
    threshold = AIR;
  } else if (humidity > MNR_O && humidity < MJR_O) {
    // Alm:MnrO
    threshold = PETRICHOR;
  } else if (humidity >= MJR_O && humidity != 127) { // notdet is > MJR_O, but we don't want to alarm mjrO unless we can read from dht22
    // Alm:MjrO
    threshold = OCEAN;
  }
}

int8_t fahrenheit(int8_t c) {
  return (1.8 * c) + 32;
}
