/**
 * Display
 *
 * @since 0.0.4
 */

#include <LiquidCrystal.h>

#include "display.h"

LiquidCrystal lcd(7, 6, 5, 4, 3, 2);

Page CURRENT_PAGE = Home;
bool EDITING = false;
uint8_t CURRENT_MENU_ITEM = 0;

void clear_screen() {
  lcd.clear();
  lcd.noBlink();
}

void set_blink_cursor_position(uint8_t y, uint8_t x) {
  lcd.setCursor(x, y);
  lcd.blink();
}

void lcd_put_char(char c) {
  static int idx = 0;

  lcd.write(c);
  idx += 1;
}

void init_lcd() {
  lcd.begin(16, 2);
  lcd.setCursor(0, 0);
//  lcd.autoscroll();
}

void paint(uint8_t cursor_line, char *text) {
  lcd.setCursor(0, cursor_line);
  lcd.print(text);
  lcd.setCursor(0, 0);
}
