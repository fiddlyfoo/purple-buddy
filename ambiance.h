/**
 * Ambiance
 *
 * @since 0.0.2
 */

/** @file */

#ifndef PURPLEBUDDY_AMBIANCE_H
#define PURPLEBUDDY_AMBIANCE_H

extern unsigned short READ_AMBIANCE_TIMEOUT;  // 300000 millis / 5 minutes
extern unsigned long READ_AMBIANCE_DEBOUNCE; // n millis -- DO NOT SET MANUALLY
extern unsigned short WRITE_AMBIANCE_TIMEOUT;
extern unsigned long WRITE_AMBIANCE_DEBOUNCE;
extern const uint8_t AMBIANCE_EEPROM_INDEX;  // 24
extern uint8_t NUM_ENTRIES;

extern byte MJR_U;
extern byte MNR_U;
extern byte COMFY;
extern byte MNR_O;
extern byte MJR_O;

extern byte h_MJR_U;
extern byte h_MNR_U;
extern byte h_MNR_O;
extern byte h_MJR_O;

extern int8_t temperature_c;
extern int8_t temperature_f;
extern byte humidity;
extern uint8_t threshold; // the current temperature alarm
extern uint8_t h_threshold; // the current humidity alarm

void update_dht22(void);
void set_h_threshold(void);
int8_t fahrenheit(int8_t c);

#endif //PURPLEBUDDY_AMBIANCE_H
