/**
 * AnalogButtons
 *
 * @since 0.0.4
 */

#ifndef ANALOGBUTTONS_H
#define ANALOGBUTTONS_H

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define MAXBUTTONS 6

class Button 
{
public:  
  int id;
  int BUTTON_L;
  int BUTTON_H;     

  // for additional functions HOLD
  long duration;
  boolean isHeldDown;  
  
  Button();
  Button(int iid, int analogLowVal, int analogHighVal, int holdDuration = 1);

  // Override these fucntions if you want
  void pressed(){ } 

  // Override these fucntions if you want 
  void held(){}
};

class AnalogButtons 
{
private:  
  // for hold button, duration is specified for each button
  long previousMillis;
  
  // AnalogPin
  int pin;
  
  // Debouncng of normal pressing
  int debounce_count;
  unsigned long time;
  int counter;
  
  // Status of last press
  int lastButtonPressed;

  Button buttons[MAXBUTTONS];
  int buttonsIndex;
  
  // registered Callback function
  void (*pt2Function)(int, boolean);    
  
public:
  AnalogButtons(int ppin, int ddebounce_count = 100, void (*pt2Func)(int, boolean) = 0);
  
  int addButton(Button b);
  
  void checkButtons();
  
  // Press each button in turn and note the values returned to 
  // Serial monitor
  static void configure(int pin);
};

#endif