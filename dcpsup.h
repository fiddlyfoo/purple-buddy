/**
 * DCPx
 *
 * @since 0.0.5
 */

#ifndef PURPLEBUDDY_DCPSUP_H
#define PURPLEBUDDY_DCPSUP_H

#include <Arduino.h>

#define DCP_DEVICE_ID 1

#define DCP_UPDRop 0x02
#define DCP_FUDRop 0x03
#define DCP_DACKop 0x06

/**
 * Opcode entry definition
 */
typedef struct DCP_OP_NTRY {
  byte text[5]; // 4-byte id (of entry, not unit) string
  byte code;    // the opcode
  byte len;     // opcode length - 2, 5, or 99
};

typedef struct DCP_CMND {
  DCP_OP_NTRY op;
  byte var1;
  byte var2;
  byte var3;
};

DCP_OP_NTRY DCP_op_lookup(byte op);
void DCP_bldCmnd(DCP_CMND *cmndP, byte op, byte v1, byte v2, byte v3);
byte DCP_genCmndBCH(char *buff, byte count);
byte DCP_calcPktLen(byte op_len, byte *buff, byte b_len);

#endif //PURPLEBUDDY_DCPSUP_H
