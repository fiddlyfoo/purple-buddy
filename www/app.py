from flask import Flask, render_template, request, jsonify
from flask_socketio import SocketIO
import socket
import smtplib


ARDUINO_PORT = 42           # the port the Arduino listens on
SERVER_HOST  = "0.0.0.0"
SERVER_PORT  = 1337

# define Flask app
app = Flask(__name__)
# set app to use websockets
socketio = SocketIO(app)

# create devices map, initialized with our single Arduino
DEVICES = [("192.168.1.13", "1")]
device_objs = {}

# create socket
s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.bind((SERVER_HOST, SERVER_PORT))
s.settimeout(3)

# set up smtp stuffs
sender = "from@fromdomain.com"
receivers = ["to@todomain.com"]
message = """From: From Person <from@fromdomain.com>
To: To Person <to@todomain.com>
Subject: {}
"""


################
# Flask stuffs #
################

@app.route("/")
def index():
    return render_template("base.html")

@app.route("/devices", methods=["POST", "GET"])
def devices():
    if request.method == "POST":
        # get the device object
        device = request.json
        # add it to the DEVICES list. In the future, disallow adding of the same ip/devid combo
        DEVICES.append((device["inet"], int(device["devid"])))

        # return the DEVICES list back to the frontend
        return jsonify(DEVICES)
    elif request.method == "GET":
        return DEVICES

@app.route("/devices/<int:device_id>", methods=["GET"])
def device(device_id):
    for device in DEVICES:
        if device[1] == device_id:
            return device
    return render_template("404.html")

@socketio.on("connect")
def handle_connect_event():
    print("Client connected...")

@socketio.on("poll")
def handle_poll_event():
    print("Polling...")

    for device in DEVICES:
        count = 0
        num_lines = 0
        fudr = 0x03
        x = DCP_bldCmnd(fudr, 0x00, 0x00, 0x00)
        y = DCP_bldPoll(int(device[1]), x, [0, 0, 0, 0, 0, 0, 0, 0])
        # y[4] = 0x00

        s.sendto(bytes(y), (device[0], ARDUINO_PORT))

        print("Current client: ", device[0])

        while True:
            try:
                data, addr = s.recvfrom(64)
                bch = DCP_genCmndBCH(data, len(data) - 2)
                rxBch = data[-1]

                if (bch == rxBch):
                    print("INFO: BCH match")
                    print("INFO: bytes received: ", data)
                    if count == 0:
                        num_lines = data[3]
                    if count > 0 and len(data) > 4:
                        print("INFO: count - ", count)
                        device_key = "{}:{}".format(addr[0], device[1])
                        device_objs[device_key] = {}
                        var1 = data[3]
                        var2 = data[4]
                        var3 = data[5]
                        var4 = data[6]
                        print("Extracted variables: var1={}, var2={}, var3={}, var4={}".format(var1, var2, var3, var4))
                        print("{}*C, {}% humidity".format(var1, var2))
                        device_objs[device_key]["ip_address"] = addr[0]
                        device_objs[device_key]["temp_c"] = var1
                        device_objs[device_key]["humidity"] = var2
                        device_objs[device_key]["threshold"] = var3
                        num_lines -= 1
                else:
                    print("Expected BCH: ", hex(bch))
                    print("Received BCH: ", hex(rxBch))
                    print("BCH mismatch... Sending as SMTP.")
                    print("Received data: ", data)
                    smtp_obj = smtplib.SMTP("localhost", 8025)
                    smtp_obj.sendmail(sender, receivers, message.format(data))
                    count = 0
                    print("Repolling")
                    handle_poll_event()
                count += 1

                if num_lines == 0:
                    count -= count
                    break
            except socket.timeout:
                break
        # s.close()
    # return jsonify(res)
    print(str(device_objs))
    socketio.emit("poll units", { "data": device_objs })


##############
# DCP stuffs #
##############

def print_hex_list(l):
    for i in range(len(l)):
        print(hex(l[i]))

class DcpOpEntry:
    def __init__(self, text, code, len):
        self.text = text
        self.code = code
        self.len  = len

class DcpCmnd:
    def __init__(self, op, var1, var2, var3):
        self.op   = op
        self.var1 = var1
        self.var2 = var2
        self.var3 = var3

DCP_op_table = [
    DcpOpEntry("UPDR", 0x02, 2),
    DcpOpEntry("FUDR", 0x03, 2),
    DcpOpEntry("DACK", 0x06, 2),
]

def DCP_op_lookup(op): # DONE
    for i in range(len(DCP_op_table)):
        if DCP_op_table[i].code == op:
            return DCP_op_table[i]
    return None

# print(DCP_op_lookup(0x02).text) # UPDR

def DCP_bldCmnd(op, v1, v2, v3):
    return DcpCmnd(op, v1, v2, v3)

def DCP_genCmndBCH(buff, count): # DONE
    nBCHPoly = 0xb8
    fBCHPoly = 0xff
    bch = 0

    for i in range(count):
        #print("i: ", i)
        bch ^= buff[i]
        for j in range(8):
            #print("j: ", j)
            if (bch & 1) == 1:
                bch = (bch >> 1) ^ nBCHPoly
            else: bch >>= 1

    bch ^= fBCHPoly

    return bch

# print(hex(DCP_genCmndBCH([0xaa, 0xfc, 0x01, 0x03], 4)))

def DCP_bldPoll(ad, cmnd, buff): # DONE
    i = 0

    buff[i] = 0xaa   # 0
    i += 1
    buff[i] = 0xfc   # 1
    i += 1
    buff[i] = ad     # 2
    i += 1
    c = DCP_op_lookup(cmnd.op)
    buff[i] = c.code # 3
    i += 1

    if c.len == 5:
        buff[i] = cmnd.var1 # 4
        i += 1
        buff[i] = cmnd.var2 # 5
        i += 1
        buff[i] = cmnd.var3 # 6
        i += 1
    # else if c.len == 99: USE THIS FOR PROJECT 6
    buff[i] = DCP_genCmndBCH(buff, i) # 7

    return buff[:i+1]

def DCP_calcPktLen(op_len, buff, b_len): # DONE
    l = 2 # adjust for frame

    if op_len == 99: # if variable length
        l += 3 # adjust for addr, op, and subop
        if l < b_len:
            l += buff[l] + 1 # data count + count byte
        else: return 0xff # buffer not long enough
    else: # not special opcode
        l += op_len # add opcode len

    l += 1 # adjust for BCH

    return l

# print(DCP_calcPktLen(2, [0xaa, 0xfc, 0x01, 0x02, 0x97], 0))

def DCP_formatTxPkt(buff, b_len):
    f_buffer = [0] * 64
    f_idx    = 0
    exp_cnt  = 0
    exp_byte = 0xaa

    for i in range(b_len): # for all buffered bytes
        if i != 0 and buff[i] == exp_byte: # if expansion byte
            if exp_byte == 0: # if first byte
                exp_byte += 1
                f_buffer[f_idx] = exp_byte
                f_idx += 1
        else:
            if exp_cnt != 0: #if exp_bytes accumulated
                f_buffer[f_idx] = exp_cnt
                f_idx += 1
                exp_cnt = 0 # re-init exp_byte counter
            f_buffer[f_idx] = buff[i]
            f_idx += 1

    if exp_cnt != 0: # write any final count of exp_bytes
        f_buffer[f_idx] = exp_cnt
    b_len = f_idx

    return f_buffer[:b_len]

def DCP_hndlResp(buff):
    return


# with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
#     count = 0
#     s.connect((ARDUINO_HOST, ARDUINO_PORT))
#     # y[-1] = 0x00 # change the bch to something bad
#     s.sendall(bytes(y))
#     # s.sendall(b'hello')
#     while True:
#         data = s.recv(1024)
#         bch = DCP_genCmndBCH(data, len(data) - 2)
#         rxBch = data[-1]
#
#         if (bch == rxBch):
#             print("Received: ", print_hex_list(data))
#             if count > 0:
#                 var1 = data[3]
#                 var2 = data[4]
#                 var3 = data[5]
#                 var4 = data[6]
#                 print("Extracted variables: var1={}, var2={}, var3={}, var4={}".format(var1, var2, var3, var4))
#                 print("{}*C, {}% humidity".format(var1, var2))
#         else:
#             print("Expected BCH: ", hex(bch))
#             print("Received BCH: ", hex(rxBch))
#             print("BCH mismatch... Doing nothing.")
#         count += 1

if __name__ == '__main__':
    socketio.run(app)
