const protocol = "http";
const domain = "localhost";
const port = 5000;
const uri = `${protocol}://${domain}:${port}`;

const parseJson = response => {
  if (!response.ok) {
    throw new Error("Network request error or misconfigured CORS");
  }

  return response.json();
};

const headers = new Headers({
  "Content-Type": "application/json",
});

/**
 * Adds a device to the localStorage
 */
function addLSDevice(k, v) {
  localStorage.setItem(k, v);
}

const Api = {
  getDevices() {
    return fetch(`${uri}/devices`, {
      method: "GET",
      headers: headers,
    })
    .then(parseJson);
  },
  getDevice(deviceId) { // not used for routing, only updating display content
    return fetch(`${uri}/devices/${deviceId}`, {
      method: "GET",
      headers: getHeaders(),
    }).then(parseJson);
  },
  createDevice(event) {
    event.preventDefault(); // prevent submit button from refreshing the page with form data in the URL
    const ipParts = document.querySelector("#inet").childNodes;

    const device = {
      devid: document.querySelector("#devid").value,
      inet: [...ipParts].filter(node => node.nodeName != "#text").map(node => node.value).join("."),
    };


    return fetch(`${uri}/devices`, {
      method: "POST",
      body: JSON.stringify(device),
      headers: headers,
    }).then(parseJson);
  },
  deleteDevice(deviceId) {
    return fetch(`${uri}/devices/${deviceId}`, {
      method: "DELETE",
      headers: headers,
    });
  },
};

function clearInputs() {
  const ip = document.getElementById("inet").childNodes;
  [...ip].map(node => { node.value = null });
  document.getElementById("devid").value = null;
}
