const Card = (deviceId, device) => `
  <div class="device-card">
    <div class="device-card__header">
      <div>
        <h3 class="device-card__title flex">Device ${deviceId.split(":")[1]}</h3>
        <span class="device-card__subhead">${device.ip_address}</span>
      </div>
      <div class="threshold-indicator alarm-dot-${device.threshold}"></div>
    </div>
    <div class="device-card__media">
      <span class="temperature">
        ${device.temp_c}&#176;C
        <span class="fahrenheit">/ ${fahrenheit(device.temp_c).toFixed(1)}&#176;F</span>
       </span>
    </div>
    <div class="device-card__content">
      <span>${device.humidity}% Humidity</span>
    </div>
  </div>
`;

const socket = io();

socket.on("connect", () => {
  console.log("Connected to socketio. Polling initial device list");
  socket.emit("poll");
});

socket.on("poll units", ({ data }) => {
  if (!Object.keys(data).length) { return; }
  console.log(data);

  const deviceList = document.querySelector("#device-list");

  const deviceIds = Object.keys(data);
  const devices = deviceIds.map(id => Card(id, data[id]));

  deviceList.innerHTML = devices;
});

const pollTimer = setInterval(() => socket.emit("poll"), 10000);

function fahrenheit(temp_c) {
  return (1.8 * temp_c) + 32;
}
