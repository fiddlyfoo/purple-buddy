/** @file */

#include <Arduino.h>
//#include <Adafruit_NeoPixel.h>
#include <EEPROM.h>

#include <DS3231_Simple.h>
#include <Wire.h>

#include "leds.h"
#include "ambiance.h"
#include "eth0.h"
#include "AnalogButtons.h"
#include "display.h"

#define ANALOG_PIN A0
#define VERSION "0.0.4"
#define BAUD_RATE 9600

typedef struct AmbianceObj { // 9 bytes in total
    DateTime dt;
    int8_t temp;     // this will always be in Celsius. Use func to convert to F
    byte humidity;
};

//Adafruit_NeoPixel pixels(3, PIN_RGB);
DS3231_Simple clock;
unsigned long MINUTE = 0;
unsigned long HOUR = 0;
const char DSPLY[] = "DSPLY \0";

void write_log() {
  short write_address = AMBIANCE_EEPROM_INDEX + (sizeof(AmbianceObj) * NUM_ENTRIES); // writes from 25 and beyond

  if (write_address < EEPROM.length()) {
    DateTime dt = clock.read();

    AmbianceObj obj;
    obj.dt = dt;
    obj.temp = temperature_c; // global temp_c (from ambiance)
    obj.humidity = humidity;  // global humidity (from ambiance)

    EEPROM.put(write_address, obj);
    NUM_ENTRIES += 1;
    EEPROM.write(24, NUM_ENTRIES);
  }
}

void clear_logs() {
  for (int i = AMBIANCE_EEPROM_INDEX; i < EEPROM.length(); i += 1) {
    EEPROM.write(i, 0);
  }

  NUM_ENTRIES = 0;
  EEPROM.write(24, NUM_ENTRIES);
}

void show_home() {
  clear_screen();

  DateTime dt;
  dt = clock.read();
  char time[16];

  sprintf(time, "%d/%d/%d    %02d:%02d", dt.Month, dt.Day, dt.Year, dt.Hour, dt.Minute);
  paint(0, time);
  paint(1, "...");
}

void show_settings(Btn btn) {
  clear_screen();

  static uint8_t page_number = 0;
  const uint8_t INCR = 2;
  const uint8_t ROWS = 16;

  char page_contents[ROWS][16] = {
    // logs page
    { "Logs      view >" }, // 0
    { "HLD SEL CLR" },
    // Packets sent/rcvd
    { "Tx/Rx" }, // 2
    { "view >" },
    // IP Address
    { "INET      edit >" }, // 4
    { }, // 5
    // Subnet
    { "Subnet    edit >" }, // 6
    { }, // 7
    // MAC
    { "Gateway   edit >" }, // 8
    { }, // 9
    { "MAC Address" }, // 10
    { },
    // Clock
    { "Clock" }, // 12
    { "set >" },
    { "Temp. Thresholds" }, // 14
    { "edit >" }
  };

  Net net = status();

  sprintf(page_contents[5], "%d.%d.%d.%d", net.ipv4[0], net.ipv4[1], net.ipv4[2], net.ipv4[3]);
  sprintf(page_contents[7], "%d.%d.%d.%d", net.subnet[0], net.subnet[1], net.subnet[2], net.subnet[3]);
  sprintf(page_contents[9], "%d.%d.%d.%d", net.gateway[0], net.gateway[1], net.gateway[2], net.gateway[3]);
  sprintf(page_contents[11], "%02X%02X%02X%02X%02X%02X", MAC_ADDRESS[0], MAC_ADDRESS[1], MAC_ADDRESS[2], MAC_ADDRESS[3], MAC_ADDRESS[4], MAC_ADDRESS[5]);

  if (btn == None) {
    page_number = 0;
//    Serial.print(DSPLY);
//    Serial.println(page_contents[page_number]);
  } else if (btn == Up) {
    if (page_number == 0) {
      page_number = ROWS - INCR;
    } else {
      page_number -= INCR;
    }

//    Serial.print(DSPLY);
//    Serial.println(page_contents[page_number]);
  } else if (btn == Down) {
    if (page_number >= ROWS - INCR) {
      page_number = 0;
    } else {
      page_number += INCR;
    }

//    Serial.print(DSPLY);
//    Serial.println(page_contents[page_number]);
  }

  CURRENT_MENU_ITEM = page_number;
  paint(0, page_contents[page_number]);
  paint(1, page_contents[page_number + 1]);
}

void prompt_input(Btn btn) {
  static uint8_t cursor_position = 0;
  clear_screen();

  static byte o[4] = { 0, 0, 0, 0 };
  char line1[16];

  if (btn == Right) {
    if (cursor_position < 14) {
      cursor_position += 1;

      // if we hit a "period" delimeter
      if (cursor_position == 3 || cursor_position == 7 || cursor_position == 11) {
        cursor_position += 1;
      }
    }
  } else if (btn == Left) {
    if (cursor_position != 0) {
      cursor_position -= 1;

      // if we hit a "period" delimeter
      if (cursor_position == 3 || cursor_position == 7 || cursor_position == 11) {
        cursor_position -= 1;
      }
    }
  } else if (btn == Up) {
    if (cursor_position == 0 || cursor_position == 4 || cursor_position == 8 || cursor_position == 12) {
      if (o[cursor_position / 4] + 100 > 255) {
        o[cursor_position / 4] = 255;
      } else {
        o[cursor_position / 4] += 100;
      }
    }

    if (cursor_position == 1 || cursor_position == 5 || cursor_position == 9 || cursor_position == 13) {
      if (o[(cursor_position - 1) / 4] + 10 > 255) {
        o[(cursor_position - 1) / 4] = 255;
      } else {
        o[(cursor_position - 1) / 4] += 10;
      }
    }

    if (cursor_position == 2 || cursor_position == 6 || cursor_position == 10 || cursor_position == 14) {
      if (o[(cursor_position - 2) / 4] + 1 > 255) {
        o[(cursor_position - 2) / 4] = 255;
      } else {
        o[(cursor_position - 2) / 4] += 1;
      }
    }
  } else if (btn == Down) {
    if (cursor_position == 0 || cursor_position == 4 || cursor_position == 8 || cursor_position == 12) {
      if (o[cursor_position / 4] - 100 < 0) {
        o[cursor_position / 4] = 0;
      } else {
        o[cursor_position / 4] -= 100;
      }
    }

    if (cursor_position == 1 || cursor_position == 5 || cursor_position == 9 || cursor_position == 13) {
      if (o[(cursor_position - 1) / 4] - 10 < 0) {
        o[(cursor_position - 1) / 4] = 0;
      } else {
        o[(cursor_position - 1) / 4] -= 10;
      }
    }

    if (cursor_position == 2 || cursor_position == 6 || cursor_position == 10 || cursor_position == 14) {
      if (o[(cursor_position - 2) / 4] - 1 < 0) {
        o[(cursor_position - 2) / 4] = 0;
      } else {
        o[(cursor_position - 2) / 4] -= 1;
      }
    }
  }

  sprintf(line1, "%03d.%03d.%03d.%03d", o[0], o[1], o[2], o[3]);
  paint(0, "Edit");
  paint(1, line1);
  set_blink_cursor_position(1, cursor_position);

  if (btn == Select) {
    Net net = status();
    Net newNet = net;

    if (CURRENT_MENU_ITEM == 4) {
      memcpy(newNet.ipv4, o, sizeof(o));
    } else if (CURRENT_MENU_ITEM == 6) {
      memcpy(newNet.subnet, o, sizeof(o));
    } else if (CURRENT_MENU_ITEM == 8) {
      memcpy(newNet.gateway, o, sizeof(o));
    }

    save_net(newNet);
    o[0] = 0; o[1] = 0; o[2] = 0; o[3] = 0;
    cursor_position = 0;

    EDITING = false;
    show_settings(None);
  }
}

void show_tx_rx() {
  clear_screen();

  char line1[16];
  char line2[16];

  sprintf(line1, "Tx: %s", SENT);
  sprintf(line2, "Rx: %s", packetBuffer);

  paint(0, line1);
  paint(1, line2);
}

void show_logs(Btn btn) {
  clear_screen();

  static uint8_t page_number = 0;
  const uint8_t INCR = 1;
  const uint8_t ROWS = 2; // will ALWAYS be 2
  char page_contents[ROWS][16];

  if (NUM_ENTRIES > 0) {
    AmbianceObj obj;
    EEPROM.get(AMBIANCE_EEPROM_INDEX + (sizeof(AmbianceObj) * page_number), obj);

    sprintf(page_contents[0], ">>%d/%d/%d %d:%d", obj.dt.Month, obj.dt.Day, obj.dt.Year, obj.dt.Hour, obj.dt.Minute);
    sprintf(page_contents[1], "%d*f/%d*c--%d%%", fahrenheit(obj.temp), obj.temp, obj.humidity);
  } else {
    sprintf(page_contents[0], "No logs yet.");
    sprintf(page_contents[1], "< back");
  }


  if (btn == None) {
    page_number = 0;
  } else if (btn == Up) {
    // if we are at the top of the page, do nothing.
    if (page_number > 0) {
      page_number -= 1;
    }
  } else if (btn == Down) {
    if (page_number < NUM_ENTRIES && NUM_ENTRIES != 0) {
      if (page_number + 1 < NUM_ENTRIES) {
        page_number += 1;
      }
    }
  }

  paint(0, page_contents[0]);
  paint(1, page_contents[1]);
}

void set_thresholds() {
  MJR_U = EEPROM.read(19);
  MNR_U = EEPROM.read(20);
  MNR_O = EEPROM.read(21);
  MJR_O = EEPROM.read(22);

  if (MJR_U == 0 && MNR_U == 0 && MNR_O == 0 && MJR_O == 0) {
    MJR_U = 60;
    MNR_U = 70;
    MNR_O = 80;
    MJR_O = 90;
  }

  EEPROM.write(19, MJR_U);
  EEPROM.write(20, MNR_U);
  EEPROM.write(21, MNR_O);
  EEPROM.write(22, MJR_O);
}

void show_temp_thresholds(Btn btn) {
  clear_screen();

  static uint8_t page_number = 0;
  char page_contents[5][16] = {
    "MJR UNDER     >",
    "MNR UNDER    <>",
    "MNR OVER     <>",
    "MJR OVER      <"
  };
  uint8_t temps[5][2] = {
    { 19, 0 },
    { 20, 0 },
    { 21, 0 },
    { 22, 0 }
  };
  char line2[16];

  temps[0][1] = EEPROM.read(temps[0][0]);
  temps[1][1] = EEPROM.read(temps[1][0]);
  temps[2][1] = EEPROM.read(temps[2][0]);
  temps[3][1] = EEPROM.read(temps[3][0]);

  if (btn == None) {
    page_number = 0;
  } else if (btn == Left) {
    if (page_number > 0) {
      page_number -= 1;
    }
  } else if (btn == Right) {
    if (page_number < 4) {
      page_number += 1;
    }
  } else if (btn == Up) {
    if (temps[page_number][1] + 1 < temps[page_number + 1][1]) {
      temps[page_number][1] += 1;

      EEPROM.write(temps[page_number][0], temps[page_number][1]);
    }
  } else if (btn == Down) {
    if ((temps[page_number][1] - 1 > temps[page_number - 1][1]) || page_number == 0) {
      temps[page_number][1] -= 1;

      EEPROM.write(temps[page_number][0], temps[page_number][1]);
    }
  }

  set_thresholds();

  paint(0, page_contents[page_number]);
  sprintf(line2, "%d", temps[page_number][1]);
  paint(1, line2);
}

void handleButtons(int id, boolean held) {
  // if we are on the home page, and button 1 is pressed,
  if (CURRENT_PAGE == Home && id == Select) {
    // go to the settings page.
    CURRENT_PAGE = Settings;
    show_settings(None);
  }
  // if we are on the settings page, and we are NOT editing,
  else if (CURRENT_PAGE == Settings && !EDITING && (id == Select || id == Left)) {
    CURRENT_PAGE = Home;
    show_home();
  }

  // handle the settings scroll
  else if (CURRENT_PAGE == Settings && !EDITING) {
    if (id == Up) {
      show_settings(Up);
    } else if (id == Down) {
      show_settings(Down);
    }
  }

  // handle temperature logs menu item and scrollable page
  if (CURRENT_PAGE == Settings && CURRENT_MENU_ITEM == 0) {
    if (id == Right) {
      show_logs(None);
      EDITING = true;
    }

    if (EDITING) {
      if (id == Left) {
        show_settings(None);
        EDITING = false;
      }

      if (id == Up) {
        show_logs(Up);
      }

      if (id == Down) {
        show_logs(Down);
      }

      if (id == Select && held) {
        clear_screen();
        paint(0, "Clearing...");

        clear_logs();
        show_settings(None);
        EDITING = false;
      }
    }
  }

  // if we are on the tx/rx menu item and the user clicks the "Right" button
  else if (CURRENT_PAGE == Settings && CURRENT_MENU_ITEM == 2) {
    if (id == Right) {
      EDITING = true;
      show_tx_rx();
    } else if (id == Left) {
      EDITING = false;
      show_settings(None);
    }
  }

  // if we are on the IP, Subnet, or Gateway menu items and the user clicks the "Right" button
  else if (CURRENT_PAGE == Settings && (CURRENT_MENU_ITEM == 4 || CURRENT_MENU_ITEM == 6 || CURRENT_MENU_ITEM == 8)) {
    if (id == Right && !EDITING) { // we reserve the Right for the next page's use
      EDITING = true;
      prompt_input(None);

    }

    if (EDITING) {
      prompt_input(id);
    }
  }

  // if we are on the Clock menu item and the user clicks the "Right" button
  else if (CURRENT_PAGE == Settings && CURRENT_MENU_ITEM == 12 && id == Right) {
    clock.promptForTimeAndDate(Serial);
  }

  // if we are on the Temp Thresholds menu item and the user clicks the "Right" button
  else if (CURRENT_PAGE == Settings && CURRENT_MENU_ITEM == 14) {
    if (id == Right && !EDITING) { // we reserve the Right for the next page's use
      EDITING = true;
      show_temp_thresholds(None);

    }

    if (EDITING) {
      if (id == Select) {
        EDITING = false;
        show_settings(None);
      } else {
        show_temp_thresholds(id);
      }
    }
  }
}

AnalogButtons analogButtons(ANALOG_PIN, 100, &handleButtons);

void blink() {
  unsigned long current_millis = millis();

  if (current_millis - HOUR >= 60000) {
    HOUR = current_millis;
    // save log every hour. do not use ds3231_simple to write.
    write_log();
  }

  if (current_millis - MINUTE >= 60000) {
    MINUTE = current_millis;
    DateTime dt;
    dt = clock.read();
    char time[16];

    sprintf(time, "%d/%d/%d    %02d:%02d", dt.Month, dt.Day, dt.Year, dt.Hour, dt.Minute);
    if (CURRENT_PAGE == Home) {
      paint(0, time);
    }
  }

  if (current_millis - READ_AMBIANCE_DEBOUNCE > READ_AMBIANCE_TIMEOUT) {
    READ_AMBIANCE_DEBOUNCE = current_millis;
    update_dht22();
    char line[16];

    sprintf(line, "%d*f/%d*c   %d%%", temperature_f, temperature_c, humidity);

    if (CURRENT_PAGE == Home) {
      paint(1, line);
    }

    /*
    if (temperature_f <= MJR_U) {
      if (threshold != PURPLE) {
        send_message("ALM: MJR U");
      }
      threshold = PURPLE;
    } else if (temperature_f > MNR_U && temperature_f <= COMFY) {
      if (threshold != BLUE) {
        send_message("ALM: MNR U");
      }
      threshold = BLUE;
    }
    if (temperature_f > COMFY && temperature_f <= MNR_O) {
      if (threshold != GREEN) {
        send_message("ALM: COMFY");
      }
      threshold = GREEN;
    }
    if (temperature_f > MNR_O && temperature_f < MJR_O) {
      if (threshold != ORANGE) {
        send_message("ALM: MNR O");
      }
      threshold = ORANGE;
    }
    if (temperature_f >= MJR_O) {
      if (threshold != RED) {
        send_message("ALM: MJR O");
      }
      threshold = RED;
    }
    */
    
    if (temperature_f <= MJR_U) {
      // Alm:MjrU
      threshold = PURPLE;
    } else if (temperature_f > MJR_U && temperature_f <= MNR_U) {
      // Alm:MnrU
      threshold = BLUE;
    } else if (temperature_f > MNR_U && temperature_f <= MNR_O) {
      // Clr:Temperature
      threshold = GREEN;
    } else if (temperature_f > MNR_O && temperature_f < MJR_O) {
      // Alm:MnrO
      threshold = ORANGE;
    } else if (temperature_f >= MJR_O && temperature_f != 127) { // notdet is > MJR_O, but we don't want to alarm mjrO unless we can read from dht22
      // Alm:MjrO
      threshold = RED;
    }

    if (CURRENT_PAGE == Settings && CURRENT_MENU_ITEM == 2 && EDITING) {
      show_tx_rx();
    }
  }

  if (current_millis - CHECK_CONNECTION_DEBOUNCE > CHECK_CONNECTION_TIMEOUT) {
    CHECK_CONNECTION_DEBOUNCE = current_millis;
    get_udp_packets(temperature_c, humidity, threshold, h_threshold);

    if (CURRENT_PAGE == Settings && CURRENT_MENU_ITEM == 2 && EDITING) {
      show_tx_rx();
    }
  }

  analogButtons.checkButtons();
}

void setup() {
  // init the RGBs and set them to a softer brightness
//  pixels.begin();
//  pixels.setBrightness(BRIGHTNESS);
  // turn the RGB power LED on
//  pixels.setPixelColor(RGB_PWR, CYAN);
//  pixels.show();

  // init the Serial
  Serial.begin(BAUD_RATE);

  // init our ethernet w/ static IP
  init_eth0_static();

  // make buttons and add them to available clickety-clacks
  Button b1 = Button(1, 725, 745);
  Button b2 = Button(2, 495, 515);
  Button b3 = Button(3, 315, 335);
  Button b4 = Button(4, 135, 155);
  Button b5 = Button(5, 0, 60);

  analogButtons.addButton(b1);
  analogButtons.addButton(b2);
  analogButtons.addButton(b3);
  analogButtons.addButton(b4);
  analogButtons.addButton(b5);

  // init the lcd
  init_lcd();

  // init the RTC
  clock.begin();
  show_home();

  // get the current values of the thresholds
  set_thresholds();

  // get the current number of entries
  NUM_ENTRIES = EEPROM.read(24); // defaults to 0
  EEPROM.write(23, 127); // this is to allow the setting of higher temp thresholds than 90.
}

void loop() {
//  read_serial_input();
  blink();
}
