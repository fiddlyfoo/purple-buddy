/**
 * LEDs
 *
 * @since 0.0.2
 */

#include <stdint.h>
#include "leds.h"

//const uint8_t PIN_RGB   = 7;
//
//const uint8_t RGB_PWR    = 0;
//const uint8_t RGB_TMP    = 1;
//const uint8_t RGB_NET    = 2;
//const uint8_t BRIGHTNESS = 1;

const uint8_t PURPLE = 0x01;
const uint8_t BLUE   = 0x02;
const uint8_t GREEN  = 0x00;
const uint8_t ORANGE = 0x03;
const uint8_t RED    = 0x04;
const uint8_t NOTDET = 0x05;

const uint8_t MIST = 0x11; // 17
const uint8_t DEW = 0x12; // 18
const uint8_t AIR = 0x00; // 0
const uint8_t PETRICHOR = 0x13; // 19
const uint8_t OCEAN = 0x14; // 20
const uint8_t H_NOTDET = 0x15; // 21

const uint32_t CYAN   = 0x00979d;
