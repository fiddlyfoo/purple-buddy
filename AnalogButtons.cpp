/**
 * AnalogButtons
 *
 * @since 0.0.4
 */

#include "AnalogButtons.h"

Button::Button() { }

Button::Button(int iid, int analogLowVal, int analogHighVal, int holdDuration) {
  id = iid;
  BUTTON_L = analogLowVal;
  BUTTON_H = analogHighVal;
  duration  = holdDuration * 1000;
}

AnalogButtons::AnalogButtons(int ppin, int ddebounce_count, void (*pt2Func)(int, boolean)) {
  pin = ppin;
  pt2Function = pt2Func;
  debounce_count = ddebounce_count;
  counter = 0;
}

int AnalogButtons::addButton(Button b) {
  if (buttonsIndex < MAXBUTTONS) {
    buttons[buttonsIndex] = b;
    buttonsIndex++;
  }
  else return -1;
}

void AnalogButtons::checkButtons() {
  // don't sample analog more than 100ms
  if (millis() + time > 100) {
    int val = analogRead(pin);

    // So we can do some reseting if no buttons were pressed
    boolean foundOne = false;

    for (int i = 0; i < buttonsIndex; i++) {
      if (val <= buttons[i].BUTTON_H + 4 && val >= buttons[i].BUTTON_L - 4 ) {
        foundOne = true;

        // First checking for button held down
        if ( buttons[i].isHeldDown != true && lastButtonPressed == buttons[i].id && ((millis() - previousMillis) > buttons[i].duration) ) {
          buttons[i].isHeldDown = true;
          buttons[i].held();
          (*pt2Function) (buttons[i].id, true);

          time = millis();
          return;
        }   // Now if a different button has been pressed
        else if (lastButtonPressed !=  buttons[i].id ) {
          if ( lastButtonPressed != buttons[i].id ) {
            counter ++;
          }
          if (counter >= debounce_count) {
            lastButtonPressed = buttons[i].id;
            counter = 0;
            previousMillis = millis();  // for detecting held
            buttons[i].pressed();
            (*pt2Function) (buttons[i].id, false);

            time = millis();
            return;
          }
        }
      }
      else { // This button was not active so it can't any longer be held down
        buttons[i].isHeldDown = false;
      }
    }
    if ( !foundOne ) lastButtonPressed = -1;
    // no button was detected they must have been released
    //lastButtonPressed = -1;
    time = millis();
  }
}

// Press each button in turn and note the values returned to
// Serial monitor
void AnalogButtons::configure(int pin) {
  // read the analog input into a variable:
  int analogValue = analogRead(pin);

  // wait 10 milliseconds for the analog-to-digital converter
  // to settle after the last reading:
  delay(10);
}