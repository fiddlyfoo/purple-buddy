/**
 * eth0
 *
 * @since 0.0.3
 */

/** @file */

#include <stdint.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

#include "dcpsup.h"

#ifndef PURPLEBUDDY_ETH0_H
#define PURPLEBUDDY_ETH0_H

typedef struct Net {
  byte ipv4[4];
  byte subnet[4];
  byte gateway[4];
};

extern byte IP_ADDRESS[4];       // 192.168.1.12
extern byte SUBNET[4];           // 192.168.1.1
extern byte GATEWAY[4];          // 255.255.255.0
extern byte MAC_ADDRESS[6];     // something, something, something, dark side
extern byte HISIP[4];           // 192.168.1.11
extern byte PORT;               // 42
extern unsigned short REMOTE_PORT; // 1337
extern bool CONNECTED;             // true if hardware and link; else false
extern const uint8_t NET_EEPROM_INDEX;     // 0

extern char packetBuffer[16];
extern char SENT[16];
extern unsigned long CHECK_CONNECTION_TIMEOUT;  // 1000 millis / 1 second
extern unsigned long CHECK_CONNECTION_DEBOUNCE; // n millis

Net status(void);
void save_net(Net net);
void init_eth0_static(void);
void send_message(char message[]);
void get_udp_packets(int8_t temp, byte humidity, uint32_t thresh);
void handle_dcp_request(byte *buff, int8_t temp, byte humidity, byte thresh_t, byte thresh_h);
void FUDR(int8_t temp, byte humidity, byte thresh_t, byte thresh_h);

#endif //PURPLEBUDDY_ETH0_H
