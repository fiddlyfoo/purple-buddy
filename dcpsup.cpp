/**
 * DCPx
 *
 * @since 0.0.5
 */

#include "dcpsup.h"

DCP_OP_NTRY DCP_op_table[] = {
//  text  | opcode    | len
  { "UPDR", DCP_UPDRop, 2 },
  { "FUDR", DCP_FUDRop, 2 },
  { "DACK", DCP_DACKop, 2 },
  { 0, 0, 0 }
};

DCP_OP_NTRY DCP_op_lookup(byte op) {
  byte i;

  for (i = 0;
       DCP_op_table[i].code != 0
       && DCP_op_table[i].code != op;
       i += 1
  );

  return (DCP_op_table[i]);
}

void DCP_bldCmnd(DCP_CMND *cmndP, byte op, byte v1, byte v2, byte v3) {
  cmndP->op = DCP_op_lookup(op);
  cmndP->var1 = v1;
  cmndP->var2 = v2;
  cmndP->var3 = v3;
}

byte DCP_genCmndBCH(char *buff, byte count) {
  byte i, j, bch, nBCHPoly = 0xb8, fBCHPoly = 0xff;

  for (bch = 0, i = 0; i < count; i += 1) {
    // Serial.println(buff[i], HEX); // => FFFFFFAA, FFFFFFFC, 1, 2, FFFFFF97
    bch ^= buff[i];

    for (j = 0; j < 8; j += 1) {
      if ((bch & 1) == 1) {
        bch = (bch >> 1) ^ nBCHPoly;
      } else {
        bch >>= 1;
      }
    }
  }

  bch ^= fBCHPoly;

  return (bch);
}

// Calculates the Tx length. For Rx length, return this - 1
byte DCP_calcPktLen(byte op_len, byte *buff, byte b_len) {
  byte len = 2; // adjust for frame

  if (op_len == 99) {
    len += 3;

    if (len < b_len) {
      len += buff[len] + 1;
    } else {
      return (0xff);
    }
  } else {
    len += op_len;
  }

  return (++len); // adjust for BCH
}


