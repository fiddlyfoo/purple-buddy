/**
 * eth0
 *
 * @since 0.0.3
 */

#include <stdint.h>
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>
#include <EEPROM.h>

#include "eth0.h"
#include "ambiance.h"

#define PIN_ETH 8

const unsigned short MAX_UDP_BUF = 16;
byte IP_ADDRESS[4]  = { 192,168,1,12 };
byte GATEWAY[4]     = { 255,255,255,0 };
byte SUBNET[4]      = { 192,168,1,1 };
//byte MAC_ADDRESS[6] = { 0x74, 0x69, 0x69, 0x2d, 0x30, 0x31 };
byte MAC_ADDRESS[6] = { 0x74, 0x69, 0x69, 0x2d, 0x30, 0x32 };
byte HISIP[4]       = { 192, 168, 1, 11 };
byte PORT           = 42;
unsigned short REMOTE_PORT = 1337;
//bool CONNECTED             = true;
EthernetUDP Udp;

char packetBuffer[MAX_UDP_BUF];
char SENT[16];

unsigned long CHECK_CONNECTION_TIMEOUT  = 3000;
unsigned long CHECK_CONNECTION_DEBOUNCE = 0;
const uint8_t NET_EEPROM_INDEX = 0;

Net status() {
  Net net;
  EEPROM.get(NET_EEPROM_INDEX, net);

  return net;
}

void save_net(Net net) {
  EEPROM.put(NET_EEPROM_INDEX, net);

  Ethernet.begin(MAC_ADDRESS, net.ipv4);

  Serial.print("set ip: ");
  Serial.println(Ethernet.localIP());
  Serial.print("set sn: ");
  Serial.println(Ethernet.subnetMask());
  Serial.print("set gw: ");
  Serial.println(Ethernet.gatewayIP());
}

void init_eth0_static() {
  Net net = status();
  Ethernet.begin(MAC_ADDRESS, IP_ADDRESS);
  //Ethernet.begin(MAC_ADDRESS, net.ipv4);
  Udp.begin(PORT);
}

void send_message(char message[]) {
  memcpy(SENT, message, sizeof(SENT));
  Udp.beginPacket(HISIP, REMOTE_PORT);
  Udp.write(message);
  Udp.endPacket();
}

void get_udp_packets(int8_t temp, byte humidity, uint32_t thresh) {
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.print("PktRcv ");
    IPAddress remote = Udp.remoteIP();
    for (int i = 0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(":");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(packetBuffer, MAX_UDP_BUF);
//    Serial.println("Contents:");
//    Serial.println(packetBuffer);

    // send a reply to the IP address and port that sent us the packet we received
//    Udp.write("got it"); // obviously handle this
//    Serial.print("Received BCH: ");
//    Serial.println((uint8_t)packetBuffer[4], HEX);
//    uint8_t exp_bch = DCP_genCmndBCH(packetBuffer, 4); // 4 because you don't include the BCH byte as part of the len
//    Serial.print("Expected BCH: ");
//    Serial.println((uint8_t)exp_bch, HEX);

//    if ((uint8_t)packetBuffer[4] == exp_bch) {
//      Serial.print("match!");
//      Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
//      Udp.write("BCH match!");
//      Udp.endPacket();
//    }

    handle_dcp_request(packetBuffer, temp, humidity, thresh);
//    Udp.endPacket();
  }
}

void handle_dcp_request(byte *buff, int8_t temp, byte humidity, byte thresh_t, byte thresh_h) {
  // 1. Check framing bytes
  byte b1 = buff[0];
  byte b2 = buff[1];
  byte addr = buff[2];
  byte opcode = buff[3];

  if (b1 != 0xaa && b2 != 0xfc) {
    return;
  }
  // 2. Expand the packet -- implement in project 6
  // 3. Check BCH
  // 3.1. Get the OP code
  DCP_OP_NTRY curr_op = DCP_op_lookup(opcode);
  // Serial.print(curr_op.code); -> 3 for FUDR
  // 3.2 Calculate the packet length
  byte len = DCP_calcPktLen(curr_op.len, buff, 0);
  // Serial.println(len); // -> 5 for UPDR/FUDR
  // 3.3 Get the rx BCH
  uint8_t rxBCH = buff[len - 1];
  uint8_t bch = DCP_genCmndBCH(buff, len - 1);

  if (rxBCH != bch) {
    sprintf(SENT, "BCH %02x %02x", rxBCH, bch);
    return;
  }
//  Serial.println(rxBCH, HEX); // => 0xf3 for FUDR
//  Serial.println(bch, HEX);   // 0xf3 also for FUDR
  // 4. Check Address matches our DCP_DEVICE_ID
  if (addr != DCP_DEVICE_ID) {
    Serial.println(F("Hmm."));
    return;
  }
  // 5. Build response -- only FUDR needs to be implemented at this time
  switch(opcode) {
    case 0x02:
      break;
    case 0x03:
      FUDR(temp, humidity, thresh_t, thresh_h);
      break;
    case 0x06:
      break;
    default:
      break;
  }
}

void FUDR(int8_t temp, byte humidity, byte thresh_t, byte thresh_h) {
  Serial.println(F("Rcv:FUDR"));
  sprintf(SENT, "FUDR");
  uint8_t count = 0x01; // increase this as more updatable fields are added
//  uint8_t opcode = 0x03;

  uint8_t line1[5] = { 0xaa, 0xfa, DCP_DEVICE_ID, count, 0x00 };
  byte bch1 = DCP_genCmndBCH(line1, 3);
  line1[4] = bch1;

  uint8_t line2[8] = { 0xaa, 0xfa, 0x01 };
  line2[3] = thresh_t;
  line2[4] = temp;
  line2[5] = thresh_h;
  line2[6] = humidity;
  line2[7] = DCP_genCmndBCH(line2, 6);

  // send the first packet
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  Udp.write(line1, 5);
  Udp.endPacket();
//  Udp.write("hey");
  // send the second packet
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  Udp.write(line2, 8);
  Udp.endPacket();
}
