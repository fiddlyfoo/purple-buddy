/**
 * Parser
 *
 * @since 0.0.4
 */

/** @file */

#ifndef PURPLEBUDDY_PARSER_H
#define PURPLEBUDDY_PARSER_H

void handle_serial(void);
void handle_backspace_input(void);

#endif //PURPLEBUDDY_PARSER_H
