/**
 * Display
 *
 * @since 0.0.4
 */

#ifndef PURPLEBUDDY_DISPLAY_H
#define PURPLEBUDDY_DISPLAY_H

enum Btn {
  None = 0,
  Select,
  Right,
  Down,
  Up,
  Left
};

enum Page {
  Logs = 0,
  Home = 98,
  Settings
};

extern uint8_t CURRENT_MENU_ITEM;
extern Page CURRENT_PAGE;
extern bool EDITING;

void clear_screen(void);
void set_blink_cursor_position(uint8_t y, uint8_t x);
void lcd_put_char(char c);
void init_lcd(void);
void paint(uint8_t cursor_line, char *text);

#endif //PURPLEBUDDY_DISPLAY_H
